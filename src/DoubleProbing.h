// Double probing.h or hashing
//  Topgyal_Gurung
// CSCI335HW3
//  Created by Topgyal on 11/26/17.
//  Copyright © 2017 topgyal. All rights reserved.


#ifndef DOUBLE_PROBING_H
#define DOUBLE_PROBING_H

#include<vector>
#include<algorithm>
#include<functional>

using namespace std;
namespace {
    int next_Prime(size_t n) { return n; }
}

// Double Probing implementation

template <typename HashedObj>
class HashTableDouble {
public:
    enum EntryType {ACTIVE, EMPTY, DELETED};
    //HashTableDouble(){}
    
    explicit HashTableDouble(size_t size = 101): array_(next_Prime(size))
    { MakeEmpty(); }
    
    bool Contains(const HashedObj & x) const {
        return IsActive(FindPos(x));
    }
    //Remove all
    void MakeEmpty() {
        current_size_ = 0;
        for (auto &entry : array_)
            entry.info_ = EMPTY;
    }
    
    bool Insert(const HashedObj & x) {
        // Insert x as active
        size_t current_pos = FindPos(x);
        if (IsActive(current_pos))
            return false;
        
        array_[current_pos].element_ = x;
        array_[current_pos].info_ = ACTIVE;
        
        // Rehash; see Section 5.5
        if (++current_size_ > array_.size() / 2)
            Rehash();
        return true;
    }
    
    bool Insert(HashedObj && x) {
        // Insert x as active
        size_t current_pos = FindPos(x);
        if (IsActive(current_pos))
            return false;
        
        array_[current_pos] = std::move(x);
        array_[current_pos].info_ = ACTIVE;
        
        // Rehash; see Section 5.5
        if (++current_size_ > array_.size() / 2)
            Rehash();
        
        return true;
    }
    
    bool Remove(const HashedObj & x) {
        size_t current_pos = FindPos(x);
        if (!IsActive(current_pos))
            return false;
        
        array_[current_pos].info_ = DELETED;
        return true;
    }
    int getCollisions(){ return num_of_collisions; }; // counter private variable
    int getNumElements(){ return current_size_; }; // return no of elements in the table
    int getTableSize(){ return array_.capacity(); }; // return table size
    
    size_t getNumProbes(const HashedObj & x) const    { // returns no of probes used for FindPos()
        size_t firstPos = InternalHash(x);
        int i = 0;
        size_t probe = firstPos; // 2nd hash
        int numProbes = 1;
        
        while (array_[probe].info_ != EMPTY &&    array_[probe].element_ != x)    {
            ++numProbes;
            ++i;
            probe = (firstPos + (i * InternalHash2(x))) % array_.capacity(); // Compute the ith double probe
        }
        return numProbes;
    }
private:
    struct HashEntry {
        HashedObj element_;
        EntryType info_;
        
        HashEntry(const HashedObj& e = HashedObj{}, EntryType i = EMPTY)
        :element_{e}, info_{i} { }
        
        HashEntry(HashedObj && e, EntryType i = EMPTY)
        :element_{std::move(e)}, info_{ i } {}
    };
    int R= 89; //R a prime for internal hash2 function  //tried with R= ..79,83,97 //same no of collision
    
    std::vector<HashEntry> array_;
    size_t current_size_;
    
    mutable int num_of_collisions=0;
    
    bool IsActive(size_t current_pos) const
    { return array_[current_pos].info_ == ACTIVE; }
    
    //FindPos diff than quadratic
    
//        size_t current_pos=InternalHash(x);
//        size_t offset=InternalHashFunc2(x);
//        while (array_[current_pos].info_ != EMPTY &&
//               array_[current_pos].element_ != x) {
//            ++num_of_collisions;  //increase by iteration
//            current_pos=current_pos+offset;
//            current_pos=current_pos %array_.size();
//            current_pos += offset;  // Compute ith probe.
//        }
//        return current_pos;
//    }
    
size_t FindPos(const HashedObj & x) const    {
     // Probe(i) = (hash(x) + i * hash2(x))% T
     size_t firstPos = InternalHash(x);
     int i = 0;
     size_t probe = firstPos; // 2nd hash
     while (array_[probe].info_ != EMPTY &&    array_[probe].element_ != x)    {
     ++num_of_collisions; // Increased in every iteration
     ++i;
     probe = (firstPos + (i * InternalHash2(x))) % array_.capacity(); // Compute the ith double probe
     }
     return probe;
}
    
    void Rehash() {
        std::vector<HashEntry> old_array = array_;
        // Create new double size empty table
        array_.resize(next_Prime(2 * old_array.size()));
        for (auto & entry : array_)
            entry.info_ = EMPTY;
        
        // Copy table over.
        current_size_ = 0;
        for (auto & entry :old_array)
            if (entry.info_ == ACTIVE)
                Insert(std::move(entry.element_));
    }
    
    size_t InternalHash(const HashedObj & x) const {
        static std::hash<HashedObj> hf;
        return hf(x) % array_.size( );
    }
    //hash function 2
    size_t InternalHash2(const HashedObj &x) const{
        static std::hash<HashedObj>hf;
        return R-(hf(x) % R);
        //return (array_.next_Prime(x)-(x*array_.size())%array_.next_Prime(x));
    }
};
#endif 

