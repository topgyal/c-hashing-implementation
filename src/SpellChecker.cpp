/*
 * SpellChecker.cpp
 *Topgyal Gurung
 * CSCI 335 HW3
 *  Created on: Nov 26, 2017
 *      Author: topgyal
 */

#include "QuadraticProbing.h"
#include "LinearProbing.h"
#include "DoubleProbing.h"

#include <iostream>
#include <cstring>
#include<string>
#include <fstream>

template <typename HashTableType>

void spellChecker(HashTableType &hash_table, const string &doc_filename, const string &dictionary_filename){
    
    cout << dictionary_filename << " is running SpellCheck on file: " << doc_filename << endl;
    
    //cout<<"after makeempty\n";
    string line;
    ifstream words(dictionary_filename); //look words in dictionary_filename

   // cout<<"\nInserting dictionary into the Hash table..\n";
    
    while(getline(words,line)){
        if(line.length()<1){continue;}
        if(line.length()>0){
            hash_table.Insert(line);
        }
    }
    
    cout<<"\n Searching for words:\n";
    
    ifstream query(doc_filename); //read line with words in doc_filename
    string input;
    while(query>>input){ // while loop to check if words in dictionary and correction
        //string manipulation
        for(int i=0; i<input.length();i++){
            if(isalpha(input[i])){input[i]=tolower(input[i]);}
            else input.erase(i,input.length()-i);
    }
    if(!hash_table.Contains(input) && input.length() > 0){
        cout << "'"<< input<<"'";
        bool match_found = false;
        
        //C) SWAP ADJACENT CHAR IN WORD
        
        for(int i=0;!match_found && i<(input.length()-1);i++){
            std::swap(input[i],input[i+1]);
            if(hash_table.Contains(input)){
                cout<<" using swap(case c) ";
              //  cout<<"\tMatch found by swapping!\n";  //for big test look messy
                match_found=true;
            }
            else{
                std::swap(input[i],input[i+1]);
            }
        } //END SWAP ADJACENT
        
        //b) REMOVE ONE CHAR FROM THE WORD
        for(int i=0;!match_found && i<(input.length());i++){
            char c=input[i];
            input.erase(input.begin()+i);
            
            if(hash_table.Contains(input)){
                cout<<" using remove(case b) ";
                //cout<<"\tMatch found by single delete!\n";  //for big test look messy
                match_found=true;
            }
            else{
                input.insert(input.begin()+i,c);
            }
        }  // END REMOVE ONE CHAR..
        
        // a) ADD ONE CHAR IN EACH POSSIBLE POSITION
        for(auto itr=input.begin();!match_found && itr!=input.end()+1;advance(itr,1)){
            for(char c='a';!match_found &&(c<='z');++c){
                input.
                insert(itr,c);
                if(hash_table.Contains(input)){
                    cout<<" using add(case a) ";
                   // cout<<"\tMatch found by single insert!\n";  //for big test look messy
                    match_found=true;
                }
                else{
                    input.erase(itr);
                }
            }
        }  //END ADD ONE CHAR..
      
        if(match_found){cout<<"-> "<<input<<endl<<endl;}
            else{cout<<" N/A"<<endl;}
        }
       
        if(query.eof()){break;}
        else
            continue;
        }
    }

  int main(int argc,char**argv){
        if(argc!=3){
            cout<<"Usage: "<<argv[0]<<"<documentfile><dictionaryfile>"<<endl;
            return 0;
        }
        string doc_filename(argv[1]);
        string dictionary_filename(argv[2]);
      //choice: quadratic
        string param_flag= "quadratic";  // param_flag(argv[3])
      
        if (param_flag == "linear") {
            HashTableLinear<string> linear_probing_table;
            cout << "\nLinear Probing is selected\n" << endl;
            spellChecker(linear_probing_table, doc_filename, dictionary_filename);
        } else if (param_flag == "quadratic") {
            HashTableQuadratic<string> quadratic_probing_table;
            cout << "\nQuadratic Probing is selected\n" << endl;
            spellChecker(quadratic_probing_table, doc_filename, dictionary_filename);
        } else if (param_flag == "double") {
            HashTableDouble<string> double_probing_table;
            cout << "\nDouble Probing is selected\n" << endl;
            spellChecker(double_probing_table, doc_filename, dictionary_filename);
        } else {
            cout << "unknown tree type " << param_flag << " only linear, quadratic, or double" << endl;
        }
    cout << endl << "End of Spelling Checking \n\n";
    return 0;
}
    


