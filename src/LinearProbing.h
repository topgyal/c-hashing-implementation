//
//  LinearProbing.h
//  Topgyal_Gurung
//  CSCI335HW3
//  Created by Topgyal on 11/26/17.
//  Copyright © 2017 topgyal. All rights reserved.
//

#ifndef LINEAR_PROBING_H
#define LINEAR_PROBING_H

#include <vector>
#include <string>
#include<algorithm>
#include <functional>

using namespace std;
namespace {
    int nextPrime(size_t n) { return n; }
}

//Linear Probing implementation

template <typename HashedObj>    //HashElement
class HashTableLinear
{
public:
    enum EntryType {ACTIVE, EMPTY, DELETED};
    
    explicit HashTableLinear(size_t size = 101): array_(nextPrime(size))
    { MakeEmpty(); }
    
    bool Contains(const HashedObj & x) const {
        return IsActive(FindPos(x));
    }
    
    void MakeEmpty() {
        current_size_ = 0;
        for (auto &entry : array_)
            entry.info_ = EMPTY;
    }
    
    bool Insert(const HashedObj & x) {
        // Insert x as active
        size_t current_pos = FindPos(x);
        if (IsActive(current_pos))
            return false;
        
        array_[current_pos].element_ = x;
        array_[current_pos].info_ = ACTIVE;
        
        // Rehash; see Section 5.5
        if (++current_size_ > array_.size() / 2)
            Rehash();
        return true;
    }
    
    bool Insert(HashedObj && x) {
        // Insert x as active
        size_t current_pos = FindPos(x);
        if (IsActive(current_pos))
            return false;
        
        array_[current_pos] = std::move(x); //
        array_[current_pos].info_ = ACTIVE;
        
        // Rehash; see Section 5.5
        if (++current_size_ > array_.size() / 2)
            Rehash();
        
        return true;
    }
    
    bool Remove(const HashedObj & x) {
        size_t current_pos = FindPos(x);
        if (!IsActive(current_pos))
            return false;
        
        array_[current_pos].info_ = DELETED;
        return true;
    }
    int getCollisions(){ return num_of_collisions; }; // counter variable
    int getNumElements(){ return current_size_; }; // return no of elements in the table
    int getTableSize(){ return array_.capacity(); }; // return table size
    
    int getNumProbes(const HashedObj &x) const    { // returns no of probes used to find the object
        int numProbes = 1; 
        int currentPos = InternalHash(x);
        
        while (array_[currentPos].info_ != EMPTY && array_[currentPos].element_ != x) {
            ++numProbes;
            currentPos += 1;  // Compute ith probe
            if (currentPos >= array_.size())
                currentPos -= array_.size();
        }
        return numProbes;
    }
    
    
private:
    struct HashEntry {
        HashedObj element_;
        EntryType info_;
        
        HashEntry(const HashedObj& e = HashedObj{}, EntryType i = EMPTY)
        :element_{e}, info_{i} { }
        
        HashEntry(HashedObj && e, EntryType i = EMPTY)
        :element_{std::move(e)}, info_{ i } {}
    };
    
    
    std::vector<HashEntry> array_;
    size_t current_size_;
    mutable int num_of_collisions=0;
    
    bool IsActive(size_t current_pos) const
    { return array_[current_pos].info_ == ACTIVE; }
    
    //FindPos
    //        int offset = 1;
    //        int current_pos = InternalHash(x);
    //
    //        while (array_[current_pos].info_ != EMPTY &&
    //               array_[current_pos].element_ != x) {
    //            current_pos += offset;  // Compute ith probe.
    //            if (current_pos >= array_.size())
    //                current_pos -= array_.size();
    //        }
    //        return current_pos;
   
    int FindPos(const HashedObj & x) const {

        int current_pos = InternalHash(x);
        
        while (array_[current_pos].info_ != EMPTY &&    array_[current_pos].element_ != x)    {
            ++num_of_collisions; // Increased in every iteration
            current_pos += 1;  // Compute ith linear probe
            if (current_pos >= array_.size())    current_pos -= array_.size();
        }
        return current_pos;
    }
    //rehash
    void Rehash() {
        std::vector<HashEntry> old_array = array_;
        
        // Create new double-sized, empty table.
        array_.resize(nextPrime(2 * old_array.size()));
        for (auto & entry : array_)
            entry.info_ = EMPTY;
        
        // Copy table over.
        current_size_ = 0;
        for (auto & entry :old_array)
            if (entry.info_ == ACTIVE)
                Insert(std::move(entry.element_));
    }
    //linear hash
    size_t InternalHash(const HashedObj & x) const {
        static std::hash<HashedObj> hf;
        return hf(x) % array_.size( );
    }
};
#endif



