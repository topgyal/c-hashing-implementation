
// Topgyal Gurung
// CSCI 335 HW3
// November 24 2017

#include "QuadraticProbing.h"
#include "LinearProbing.h"
#include "DoubleProbing.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;


template <typename HashTableType>
void TestFunctionForHashTable(HashTableType &hash_table, const string &words_filename, const string &query_filename) {
    std:: cout << "\nTestFunctionForHashTables: "<<endl;
    std::cout << "Words filename: " << words_filename <<endl;
    std::cout << "Query filename: " << query_filename <<endl;
    hash_table.MakeEmpty();
    
    //Read all words from txt file and insert into hashtable
   
    ifstream in_stream(words_filename);  //read lines
     string word_line;  //string takes word from file
    
    while(getline(in_stream,word_line)){
        if(word_line.length()<1){continue;} // if empty lines
        if(word_line.length()>0){hash_table.Insert(word_line);}  //insert into hash table
    }
    //print C,N,N/T,C/N
    cout << "\nCollisions: " <<hash_table.getCollisions()<<endl;
    cout << "Number of items: " <<hash_table.getNumElements()<< endl;
    cout << "Size of hash table: " <<hash_table.getTableSize()<<endl;
    cout << "Load factor: " <<(float)hash_table.getNumElements()/(float)hash_table.getTableSize() <<endl;
    cout << "Avg. number of collisions: " <<(float)hash_table.getCollisions()/(float)hash_table.getNumElements()<< endl;
           
           //search for  words from query file in hash_table
           cout<<"\nCheck words in Query file \n";
           ifstream query(query_filename); //read lines of words in query_file
           string in;
           
           while(query>>in){
               int probes=hash_table.getNumProbes(in);
               cout<<endl;
               if(!hash_table.Contains(in)){cout<<"'"<<in<<"' Not Found. ";}
               else
                   cout<<"'"<<in<<"' Found. ";
               cout<<"Number of Probes used: "<<probes;
               if(query.eof()){break;}
               else continue;
           }
           
        }
           
           // Sample main for program CreateAndTestHash
           int main(int argc, char **argv) {
               if (argc != 4) {
                   cout << "Usage: " << argv[0] << " <wordsfilename> <queryfilename> <flag>" << endl;
                   return 0;
               }
               
               const string words_filename(argv[1]);
               const string query_filename(argv[2]);
               const string param_flag(argv[3]);
               
               if (param_flag == "linear") {
                   cout<<" \nLinear Probing: "<<endl;
                   HashTableLinear<string> linear_probing_table;
                   TestFunctionForHashTable(linear_probing_table, words_filename, query_filename);
               } else if (param_flag == "quadratic") {
                   cout<<"\nQuadratic Probing: "<<endl;
                   HashTableQuadratic<string> quadratic_probing_table;
                   TestFunctionForHashTable(quadratic_probing_table, words_filename, query_filename);
                   
               } else if (param_flag == "double") {
                   cout<<"\nDouble Hashing: "<<endl;
                   HashTableDouble<string> double_probing_table;
                   TestFunctionForHashTable(double_probing_table, words_filename, query_filename);
                   
               } else {
                   cout << "\nUknown tree type " << param_flag << " (User should provide linear, quadratic, or double)" << endl;
               }
               cout<<" \n\n End of the Probing test\n\n";
               return 0;
           }

