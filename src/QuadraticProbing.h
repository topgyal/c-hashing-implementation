
//QuadraticProbing.h
//  Topgyal_Gurung
//  CSCI335HW3
//  Created by Topgyal on 11/26/17.
//  Copyright © 2017 topgyal. All rights reserved.

#ifndef QUADRATIC_PROBING_H
#define QUADRATIC_PROBING_H

#include<iostream>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;

namespace {
    // Internal method to test if a positive number is prime.
    bool IsPrime(size_t n) {
        if( n == 2 || n == 3 )
            return true;
        
        if( n == 1 || n % 2 == 0 )
            return false;
        
        for( int i = 3; i * i <= n; i += 2 )
            if( n % i == 0 )
                return false;
        
        return true;
    }
    
    
    // Internal method to return a prime number at least as large as n.
    size_t NextPrime(size_t n) {     //size_t
        if (n % 2 == 0)
            ++n;
        while (!IsPrime(n)) n += 2;
        return n;
    }
    
}  // namespace

// Quadratic probing implementation.
//hashfunction hf(x)=(x%tablesize)
//if active, look next empty position; hf(x)=(x+i^2)%tablesize)

template <typename HashedObj>
class HashTableQuadratic {
public:
    enum EntryType {ACTIVE, EMPTY, DELETED};
    
    explicit HashTableQuadratic(size_t size = 101):array_(NextPrime(size))
    { MakeEmpty(); }  //set default size 101,otherwise first prime after size
    
    bool Contains(const HashedObj & x) const {  //return if x is present
        return IsActive(FindPos(x));
    }
    //remove all items
    //lazy deletion
    void MakeEmpty() {
        current_size_ = 0;
        for (auto &entry : array_)
            entry.info_ = EMPTY;
    }
    
    bool Insert(const HashedObj & x) {  //insert x into table
        // Insert x as active
        
        size_t current_pos = FindPos(x);
        if (IsActive(current_pos)){  //if already exists, return false
            return false;
        }
        array_[current_pos].element_ = x;
        array_[current_pos].info_ = ACTIVE;
        
        // Rehash; see Section 5.5
        if (++current_size_ > array_.size() / 2) //if table more than half full
            Rehash();
        return true;
    }
    bool Insert(HashedObj && x) {
        // Insert x as active
        size_t current_pos = FindPos(x);
        if (IsActive(current_pos))
            return false;
        
        array_[current_pos] = std::move(x);
        array_[current_pos].info_ = ACTIVE;
        
        // Rehash; see Section 5.5
        if (++current_size_ > array_.size() / 2)
            Rehash();
        
        return true;
    }
    
    size_t FindPos(const HashedObj & x) const {
        size_t offset = 1;
        size_t current_pos = InternalHash(x);
        
        //assuming table half empty and table size is prime
        while (array_[current_pos].info_ != EMPTY &&
               array_[current_pos].element_ != x) {
            current_pos += offset;  // Compute ith probe.
            offset += 2;
            if (current_pos >= array_.size()) current_pos -= array_.size();
        }
        return current_pos;
    }
    
//    size_t *FindPos(const HashedObj &x) const{
//        //return pointer to result. Result[0]=final index, Result[1] holds noOfProbes
//        size_t *result=new size_t[2];
//        size_t currentPos = InternalHash( x );
//        result[1] = 0;
//
//
//        while( array_[ currentPos ].info_ != EMPTY &&
//              array_[ currentPos ].element != x )
//        {
//            currentPos++;  // Compute ith probe
//            result[1]++;
//            if( currentPos >= array_.size( ) )
//                currentPos -= array_.size( );
//        }
//
//        result[0] = currentPos;
//        return result;
//        }
    
    //internal hash
    //finds initial hash val of obj x(hash% table size)
    size_t InternalHash(const HashedObj & x) const {
        static std::hash<HashedObj> hf;
        return hf(x) % array_.size( );
    }
    
    bool Remove(const HashedObj & x) {  //remove obj x
        size_t current_pos = FindPos(x);
        if (!IsActive(current_pos))   //if obj doesnt exist,return false
            return false;
        array_[current_pos].info_ = DELETED; //otherwise mark the spot deleted
        return true;
    }
    int getCollisions(){ return num_of_collisions; }; // counter private variable
    int getNumElements(){ return current_size_; }; // return no of elements in the table
    int getTableSize(){ return array_.capacity(); }; // return table size
    
    int getNumProbes(const HashedObj & x) const { // returns probes used to FindPos
        int offset = 1;
        int current_pos = InternalHash(x);
        int numProbes = 1;
        
        while (array_[current_pos].info_ != EMPTY && array_[current_pos].element_ != x) {
            numProbes++;
            current_pos += offset;  // Compute i th probe.
            offset += 2;
            if (current_pos >= array_.size())
                current_pos -= array_.size();
        }
        return numProbes;
    }
    
private:
    struct HashEntry {  //hash element struct, hold element and EntryType
        HashedObj element_;
        EntryType info_;
        
        //constructor for hashentry
        HashEntry(const HashedObj& e = HashedObj{}, EntryType i = EMPTY)
        :element_{e}, info_{i} { }
        
        HashEntry(HashedObj && e, EntryType i = EMPTY)
        :element_{std::move(e)}, info_{ i } {}
    };
    
    
    std::vector<HashEntry> array_;
    size_t current_size_;
    
    mutable int num_of_collisions=0; //mutable as increased in FindPos
    
    //return if position is active or full
    bool IsActive(size_t current_pos) const
    { return array_[current_pos].info_ == ACTIVE; }
    
    
    
    void Rehash() {
        std::vector<HashEntry> old_array = array_;
        
        // Create new double-sized, empty table.
        array_.resize(NextPrime(2 * old_array.size()));
        for (auto & entry : array_)
            entry.info_ = EMPTY;
        
        // Copy table over.
        current_size_ = 0;
        for (auto & entry :old_array)
            if (entry.info_ == ACTIVE)
                Insert(std::move(entry.element_));
    }
    
};

#endif


