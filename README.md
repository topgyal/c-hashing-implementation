# hashing

Topgyal Gurung

Nov 26 2017

CSCI 335-03 Assignment 3

Description: three hashing implementation


- Parts complete/incomplete

All parts of this assignments are complete

- Build instructions for makefile : 

Unzip all the files in a same directory and run make all command in the linux terminal.

I chose R= 89, a prime number close to 101.

- Description of the known problems:

I put my SpellCheck program SpellChecker.cpp. To test the program, the valid input command will be:
./SpellChecker document1_short.txt words.txt

- Input files: Linear,Quadratic and Double Probing header files
               CreateAndTestHash.cpp SpellChecker.cpp and text files
- Output file: SpellChecker, CreateAndTestHash executable and other object files
- Compile file: Makefile
